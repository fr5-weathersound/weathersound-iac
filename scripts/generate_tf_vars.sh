#!/bin/bash

echo -n "public_key = " > IaC/deployment.tfvars
echo  "\"$(cat ~/.ssh/id_rsa.pub)\"" >> IaC/deployment.tfvars
echo "project_name = \"$PROJECT_NAME\"" >> IaC/deployment.tfvars