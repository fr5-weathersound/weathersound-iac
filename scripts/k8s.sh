$VPC_CIDR="172.16.0.0"
$MASK="16"
$CP_IP="172.16.100.1"

sudo apt update
sudo apt -y upgrade && sudo systemctl reboot
sudo apt update
sudo apt -y install curl apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt -y install vim git curl wget kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

# tester
kubectl version --client && kubeadm version

sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo swapoff -a

# Enable kernel modules
sudo modprobe overlay
sudo modprobe br_netfilter

# Add some settings to sysctl
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

# Reload sysctl
sudo sysctl --system

# Ensure you load modules
sudo modprobe overlay
sudo modprobe br_netfilter

# Set up required sysctl params
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

# Reload sysctl
sudo sysctl --system
#################################
# Add Cri-o repo
sudo su -
OS="xUbuntu_20.04"
VERSION=1.22
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list
curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | apt-key add -
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | apt-key add -

# Update CRI-O CIDR subnet
# 
sudo sed -i "s/10.85.0.0/$VPC_CIDR/g" /etc/cni/net.d/100-crio-bridge.conf

# Install CRI-O
sudo apt update
sudo apt install cri-o cri-o-runc

# Start and enable Service
sudo systemctl daemon-reload
sudo systemctl restart crio
sudo systemctl enable crio
sudo systemctl status crio

################## MASTER
lsmod | grep br_netfilter
sudo systemctl enable kubelet
sudo kubeadm config images pull
# sudo kubeadm config images pull --cri-socket /var/run/crio/crio.sock
echo "$CP_IP    weathersound.k8s.com" >>  /etc/hosts
sudo kubeadm init \
    --pod-network-cidr="$VPC_CIDR/$MASK" \
    --cri-socket /var/run/crio/crio.sock \
    --upload-certs \
    --control-plane-endpoint=weathersound.k8s.com
#   register: output
# For nodes
#   - name: Storing Logs and Generated token for future purpose.
#     local_action: copy content={{ output.stdout }} dest={{ token_file }}
#     become: False


mkdir -p $HOME/.kube
sudo cp -f /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Network
kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml 
kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml

# Confirm that node is ready
kubectl get nodes -o wide


################## NODES
# sudo kubeadm config images pull --cri-socket /var/run/crio/crio.sock
echo "$CP_IP    weathersound.k8s.com" >>  /etc/hosts

#   - name: Copying token to worker nodes
#     copy: src={{ token_file }} dest=join_token

#   - name: Joining worker nodes with kubernetes master
#     shell: |
#      kubeadm reset -f
#      cat join_token | tail -2 > out.sh
#      sh out.sh
