resource "aws_key_pair" "instance_key" {
  key_name   = "${var.project_name}instance-key-2"
  public_key = var.public_key
}