
resource "aws_lb" "front_end" {
  name               = "${var.project_name}-ALB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [
    aws_security_group.alb.id,
    aws_security_group.all_access_internally.id
  ]

  subnets            = [
    aws_subnet.p_public_subnet_1.id,
    aws_subnet.p_public_subnet_2.id
  ]

  enable_deletion_protection = false

  depends_on = [
    aws_internet_gateway.p_ig,
  ]
  
  tags = {
    Environment = "production"
    Project = var.project_name
  }
}


resource "aws_lb_target_group" "front_end" {
  name     = "${var.project_name}-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.p_vpc.id
}


resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.front_end.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front_end.arn
  }
}

## Iterate on instances
resource "aws_lb_target_group_attachment" "frontend_tg_att" {
  target_group_arn = aws_lb_target_group.front_end.arn
  target_id        = aws_instance.node_1.id
  port             = 80
  depends_on = [
    aws_instance.node_1,
    aws_instance.node_2,
    aws_instance.node_3
  ]
}

resource "aws_lb_target_group_attachment" "frontend_tg_att_2" {
  target_group_arn = aws_lb_target_group.front_end.arn
  target_id        = aws_instance.node_2.id
  port             = 80
  depends_on = [
    aws_instance.node_1,
    aws_instance.node_2,
    aws_instance.node_3
  ]
}

resource "aws_lb_target_group_attachment" "frontend_tg_att_3" {
  target_group_arn = aws_lb_target_group.front_end.arn
  target_id        = aws_instance.node_3.id
  port             = 80
  depends_on = [
    aws_instance.node_1,
    aws_instance.node_2,
    aws_instance.node_3
  ]
}