
resource "aws_vpc" "p_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "${var.project_name}_vpc_2"
    Project_Name = "${var.project_name}"
  }

}

resource "aws_internet_gateway" "p_ig" {
  vpc_id = aws_vpc.p_vpc.id

  tags = {
    Name = "${var.project_name}_internetgateway"
    Project_Name = "${var.project_name}"
  }
}


