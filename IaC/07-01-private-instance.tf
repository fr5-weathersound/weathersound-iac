#######
### Instance 1
#######

resource "aws_network_interface" "p_interface_node_1" {
  subnet_id   = aws_subnet.p_private_subnet_1.id
  private_ips = ["172.16.21.100"]
  security_groups = [aws_security_group.all_access_internally.id]
  tags = {
    Name = "${var.project_name}_p_interface_node_1"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_instance" "node_1" {
  ami           = data.aws_ami.instance_image.id
  instance_type = "t2.medium"

  tags = {
    Name = "${var.project_name}-Node-1"
    Project_Name = "${var.project_name}"
  }
  key_name = aws_key_pair.instance_key.key_name

  network_interface {
    network_interface_id = aws_network_interface.p_interface_node_1.id
    device_index         = 0
  }
  
  depends_on = [aws_instance.bastian]

  credit_specification {
    cpu_credits = "unlimited"
  }
}

#######
### Instance 2
#######

resource "aws_network_interface" "p_interface_node_2" {
  subnet_id   = aws_subnet.p_private_subnet_1.id
  private_ips = ["172.16.21.101"]
  security_groups = [aws_security_group.all_access_internally.id]
  tags = {
    Name = "${var.project_name}_p_interface_node_2"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_instance" "node_2" {
  ami           = data.aws_ami.instance_image.id
  instance_type = "t2.medium"

  tags = {
    Name = "${var.project_name}-Node-2"
    Project_Name = "${var.project_name}"
  }
  key_name = aws_key_pair.instance_key.key_name

  network_interface {
    network_interface_id = aws_network_interface.p_interface_node_2.id
    device_index         = 0
  }
  
  depends_on = [aws_instance.bastian]

  credit_specification {
    cpu_credits = "unlimited"
  }
}

#######
### Instance 3
#######

resource "aws_network_interface" "p_interface_node_3" {
  subnet_id   = aws_subnet.p_private_subnet_2.id
  private_ips = ["172.16.22.100"]
  security_groups = [aws_security_group.all_access_internally.id]
  tags = {
    Name = "${var.project_name}_p_interface_node_3"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_instance" "node_3" {
  ami           = data.aws_ami.instance_image.id
  instance_type = "t2.medium"

  tags = {
    Name = "${var.project_name}-Node-3"
    Project_Name = "${var.project_name}"
  }
  key_name = aws_key_pair.instance_key.key_name

  network_interface {
    network_interface_id = aws_network_interface.p_interface_node_3.id
    device_index         = 0
  }
  
  depends_on = [aws_instance.bastian]

  credit_specification {
    cpu_credits = "unlimited"
  }
}
