
data "aws_route53_zone" "domain_name" {
  name         = "weathersound.fr"
}

resource "aws_route53_record" "no_prefix" {
  zone_id = data.aws_route53_zone.domain_name.zone_id
  name    = "weathersound.fr"
  type    = "A"

  alias {
    name                   = aws_lb.front_end.dns_name
    zone_id                = aws_lb.front_end.zone_id
    evaluate_target_health = true
  }

}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.domain_name.zone_id
  name    = "www.weathersound.fr"
  type    = "A"

  alias {
    name                   = aws_lb.front_end.dns_name
    zone_id                = aws_lb.front_end.zone_id
    evaluate_target_health = true
  }

}

resource "aws_route53_record" "all_sub_domains" {
  zone_id = data.aws_route53_zone.domain_name.zone_id
  name    = "*.weathersound.fr"
  type    = "A"

  alias {
    name                   = aws_lb.front_end.dns_name
    zone_id                = aws_lb.front_end.zone_id
    evaluate_target_health = true
  }

}