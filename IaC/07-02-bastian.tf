# Bastian Instance Interface
resource "aws_network_interface" "p_interface_bastian" {
  subnet_id   = aws_subnet.p_public_subnet_1.id
  private_ips = ["172.16.11.100"]
  source_dest_check = false
  security_groups = [
    aws_security_group.bastian.id, 
    aws_security_group.all_access_internally.id
  ]

  tags = {
    Name = "${var.project_name}_p_interface_bastian"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_instance" "bastian" {
  ami           = data.aws_ami.instance_image.id
  instance_type = "t2.micro"

  
  tags = {
    Name = "${var.project_name}-Bastian"
    Project_Name = "${var.project_name}"
  }

  key_name = aws_key_pair.instance_key.key_name

  network_interface {
    network_interface_id = aws_network_interface.p_interface_bastian.id
    device_index         = 0
  }
  
  depends_on = [aws_internet_gateway.p_ig]

  credit_specification {
    cpu_credits = "unlimited"
  }
}

