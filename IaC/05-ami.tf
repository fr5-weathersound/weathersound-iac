
data "aws_ami" "instance_image" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.image.format_pattern]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = var.image.owners # Canonical
}
