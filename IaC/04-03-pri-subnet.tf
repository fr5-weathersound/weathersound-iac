
resource "aws_subnet" "p_private_subnet_1" {
  vpc_id            = aws_vpc.p_vpc.id
  cidr_block        = "172.16.21.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.project_name}_private_subnet_1"
    Type = "Private"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_subnet" "p_private_subnet_2" {
  vpc_id            = aws_vpc.p_vpc.id
  cidr_block        = "172.16.22.0/24"
  availability_zone = "us-east-2c"
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.project_name}_private_subnet_2"
    Type = "Private"
    Project_Name = "${var.project_name}"
  }
}

# Routing - For private Subnet

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.p_vpc.id}"
  tags = {
    Name        = "${var.project_name}-private-route-table"
    Project_Name = "${var.project_name}"
  }
}


resource "aws_route" "nat_route" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = "${aws_network_interface.p_interface_bastian.id}"

  depends_on = [
    aws_network_interface.p_interface_bastian,
  ]

  timeouts {
    create = "5m"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.p_private_subnet_1.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "private_2" {
  subnet_id      = aws_subnet.p_private_subnet_2.id
  route_table_id = aws_route_table.private.id
}