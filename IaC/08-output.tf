resource "null_resource" "list_instance_ips" {
  depends_on = [
    aws_instance.bastian,
    aws_instance.node_3,
    aws_instance.node_2,
    aws_instance.node_1
  ]

  provisioner "local-exec" {
    command = <<-EOT
      echo -n "" > ../tmp/public_ips.txt
      echo -n "" > ../tmp/private_ips.txt
      echo -n "" > ../tmp/cp_private_ips.txt
      echo ${aws_instance.bastian.public_ip} >> ../tmp/public_ips.txt
      echo ${aws_instance.node_1.private_ip} >> ../tmp/cp_private_ips.txt
      echo ${aws_instance.node_2.private_ip} >> ../tmp/private_ips.txt
      echo ${aws_instance.node_3.private_ip} >> ../tmp/private_ips.txt
    EOT
  }
}
