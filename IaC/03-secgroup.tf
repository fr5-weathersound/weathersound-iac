resource "aws_security_group" "allow_ssh_and_http" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.p_vpc.id

  ingress {
    description      = "HTTP from *"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "SSH from *"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
    Project = "${var.project_name}"
  }
}

resource "aws_security_group" "all_access_internally" {
  name        = "all_access_internally"
  description = "Allow all from sec group traffic"
  vpc_id      = aws_vpc.p_vpc.id

  ingress {
    description      = "TCP from *"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [aws_vpc.p_vpc.cidr_block]
    # security_groups  = [aws_security_group.all_access_internally.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "all_access_internally"
    Project = "${var.project_name}"
  }
}

resource "aws_security_group" "bastian" {
  name        = "Bastian Secgroup"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.p_vpc.id

  ingress {
    description      = "SSH from *"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "all_access_internally"
  }
}

resource "aws_security_group" "alb" {
  name        = "ALB Secgroup"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.p_vpc.id

  ingress {
    description      = "HTTP from *"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.project_name}_ALB_Sec_group"
    Project =   "${var.project_name}"  
  }
}