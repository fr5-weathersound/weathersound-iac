variable "project_name" {  
    type    = string  
    default = "weathersound_2"
}

variable "public_key" {  
    type    = string  
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZwkLU85j2cSyIPKx2GGHyfYdtblDlOcxMvMzh4Ld+oHJ9V7W59OG5lyXzNbNIdvpSPRzgZtIxYa6lpiIxMA40nCeh45oJr8riGUKFX5eUa0hOHFgk3R6kJwxmr5Qlp/5Ux+OytIZX8nmubf7XqX7umxstw63GUlRbl9aCFhwIYYjQTkpQU+arAwYbVhiMIcx4OoLpEW/We9yDzdQXUrPDafeGrHGhrFRI2xKiXMpwxFIB2LaexSxZrR3vcNjbDLB9eHu5GDpNg3pewwPnAqPJmsHPzLaHFxh13rCfwymjK9U92N4ynwPnof3LpDLERrSXLNF71Xsqc0mTeMKCOGzd weathersound-dev"
}

variable "image" {  
    type    = object({
        format_pattern: string
        owners: list(string)
    }) 
    default = {
        format_pattern =  "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
        owners = ["099720109477"]
    }
}
