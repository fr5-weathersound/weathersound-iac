resource "aws_subnet" "p_public_subnet_1" {
  vpc_id            = aws_vpc.p_vpc.id
  cidr_block        = "172.16.11.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.project_name}_public_subnet_1"
    Type = "Public"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_subnet" "p_public_subnet_2" {
  vpc_id            = aws_vpc.p_vpc.id
  cidr_block        = "172.16.12.0/24"
  availability_zone = "us-east-2b"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.project_name}_public_subnet_2"
    Type = "Public"
    Project_Name = "${var.project_name}"
  }
}

# Routing - For public Subnet
resource "aws_default_route_table" "public_rt" {
  default_route_table_id = "${aws_vpc.p_vpc.main_route_table_id}"

  tags = {
    Name = "${var.project_name}_subnet-rt-public"
    Project_Name = "${var.project_name}"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_default_route_table.public_rt.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.p_ig.id}"

  timeouts {
    create = "5m"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.p_public_subnet_1.id
  route_table_id = aws_default_route_table.public_rt.id
}

resource "aws_route_table_association" "public_2" {
  subnet_id      = aws_subnet.p_public_subnet_2.id
  route_table_id = aws_default_route_table.public_rt.id
}