#!/bin/bash
# Commands: plan | apply | destroy"

export PROJECT_NAME="weathersound2"
mkdir tmp

# Load AWS Credentials
source ./aws-cred-export.sh

# Generate Terraform Variables
source ./scripts/generate_tf_vars.sh

# Deploy Terraform
terraform -chdir=IaC $1 -var-file="deployment.tfvars"

# Export Ansible Host file
export hosts=$(cat tmp/public_ips.txt)
export cp=$(cat tmp/cp_private_ips.txt)
export nodes=$(cat tmp/private_ips.txt)
cat templates/hosts.template | envsubst '${hosts}, ${nodes}, ${cp}' > playbook/hosts

# If apply, run Ansible
if [ "$1" = "apply" ]; then

    export ANSIBLE_NOCOWS=1
    # This will run all playbooks but config playbook - for installing packages
    echo "Sleeping for 10 seconds ..."
    # Sleep to wait for instances to be configured
    sleep 10
    echo "Starting Ansible ..."
    cd playbook

    # Install NAT Instance - Letting private subnet have internet access
    echo "  Configuring NAT Instance - Bastian ..."
    ansible-playbook $(ls | grep -E -v "vars\.yml" | grep "yml$" | tr '\n' ' ') -i hosts -e @ansible-vars.vars.yml --limit "bastian"
    echo "  Configuring K8S ......................"
    ansible-playbook $(ls | grep -E -v "vars\.yml" | grep "yml$" | tr '\n' ' ') -i hosts -e @ansible-vars.vars.yml

fi
#rm -rf tmp