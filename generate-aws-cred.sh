#!/bin/bash

echo -n "AWS Access ID: "
read AWS_ACCESS_KEY_ID
export AWS_ACCESS_KEY_ID
echo -n "AWS Secret Access key: "
read AWS_SECRET_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY

cat templates/aws-cred-export.sh.template | envsubst '${AWS_ACCESS_KEY_ID}, ${AWS_SECRET_ACCESS_KEY}' > aws-cred-export.sh